import dash
import dash_core_components as dcc
import dash_table
import dash_html_components as html
from dash.dependencies import Input, Output
import pickle
import plotly.graph_objs as go
import chart_studio
from plotly.graph_objs import *
import chart_studio.plotly as py
import random
chart_studio.tools.set_credentials_file(username='legard9', api_key='HlqO8DxyJd2tIevOfhf9')

alliances = [37,102,146,185,27,122,58,80]
def plotly_figure(graph_messages, show_boss):
    labels=list(graph_messages.vs['label'])
    n_nodes=len(labels)

    layt=graph_messages.layout_fruchterman_reingold() #kamada-kawai layout

    Xn1 = []; Yn1 = []; Xn2 = []; Yn2 = []
    labels1 = []; labels2 = []
    if(show_boss):
        for k in range(0,n_nodes):
            node = graph_messages.vs[k]
            if(node['is_boss']!=1):
                Xn1.append(layt[k][0])
                Yn1.append(layt[k][1])
                labels1.append(labels[k])
            else:
                Xn2.append(layt[k][0])
                Yn2.append(layt[k][1])
                labels2.append(labels[k])
    else:
        for k in range(0,n_nodes):
            Xn1.append(layt[k][0])
            Yn1.append(layt[k][1])
        labels1 = labels

    edge_list=[e.tuple for e in graph_messages.es]

    # extraxt edges positions
    Xe1=[]; Ye1=[]
    for e in edge_list:
        Xe1+=[layt[e[0]][0],layt[e[1]][0], None]
        Ye1+=[layt[e[0]][1],layt[e[1]][1], None]

    #plot edges and nodes

    marker1=Scatter(x=Xn1,
                y=Yn1,
                mode='markers',
                name='ntw',
                marker=dict(symbol='circle-dot',
                                            size=10,
                                            color='#6959CD',
                                            line=dict(color='rgb(50,50,50)', width=0.5)
                                            ),
                text=labels1,
                hoverinfo='text'
                )
    
    marker2=Scatter(x=Xn2,
            y=Yn2,
            mode='markers',
            name='ntw',
            marker=dict(symbol='circle-dot',
                                        size=15,
                                        color='#ff0000',
                                        line=dict(color='rgb(50,50,50)', width=0.5)
                                        ),
            text=labels2,
            hoverinfo='text'
            )

    line1=Scatter(x=Xe1,
                y=Ye1,
                mode='lines',
                line= dict(color='rgb(210,210,210)', width=1),
                hoverinfo='none'
                )

    # Hide axis information
    axis=dict(showline=False, 
            zeroline=False,
            showgrid=False,
            showticklabels=False,
            title='' 
            )

    width=800
    height=800
    layout_plotly=Layout(title= "Scambio messaggi intra-alleanza",  
        font= dict(size=12),
        showlegend=False,
        autosize=False,
        width=width,
        height=height,
        xaxis=layout.XAxis(axis),
        yaxis=layout.YAxis(axis),          
        margin=layout.Margin(
            l=40,
            r=40,
            b=85,
            t=100,
        ),
        hovermode='closest'        
        )

    data=[line1, marker1, marker2]
    return Figure(data=data, layout=layout_plotly)

def plotly_figure2(graph_messages, show_boss):
    labels=list(graph_messages.vs['label'])
    n_nodes=len(labels)

    layt=graph_messages.layout_fruchterman_reingold() #kamada-kawai layout

    Xn1 = []; Yn1 = []; Xn2 = []; Yn2 = []
    labels1 = []; labels2 = []
    bosses = []
    if(show_boss):
        for k in range(0,n_nodes):
            node = graph_messages.vs[k]
            if(node['is_boss']!=1):
                Xn1.append(layt[k][0])
                Yn1.append(layt[k][1])
                labels1.append(labels[k])
            else:
                bosses.append(node['label'])
                Xn2.append(layt[k][0])
                Yn2.append(layt[k][1])
                labels2.append(labels[k])
    else:
        for k in range(0,n_nodes):
            Xn1.append(layt[k][0])
            Yn1.append(layt[k][1])
        labels1 = labels

    bosses_ids = []
    for el in range(0,len(graph_messages.vs)):
        v = graph_messages.vs[el] 
        if v['label'] in bosses:
            bosses_ids.append(el)

    edge_list=[e.tuple for e in graph_messages.es]

    # extraxt edges positions
    Xe1=[]; Ye1=[]; Xe2=[]; Ye2=[]
    for e in edge_list:
        if(e[0] in bosses_ids and e[1] in bosses_ids):
            Xe1+=[layt[e[0]][0],layt[e[1]][0], None]
            Ye1+=[layt[e[0]][1],layt[e[1]][1], None]
        else:
            Xe2+=[layt[e[0]][0],layt[e[1]][0], None]
            Ye2+=[layt[e[0]][1],layt[e[1]][1], None]

    #plot edges and nodes

    marker1=Scatter(x=Xn1,
                y=Yn1,
                mode='markers',
                name='ntw',
                marker=dict(symbol='circle-dot',
                                            size=10,
                                            color='#6959CD',
                                            line=dict(color='rgb(50,50,50)', width=0.5)
                                            ),
                text=labels1,
                hoverinfo='text'
                )
    
    marker2=Scatter(x=Xn2,
            y=Yn2,
            mode='markers',
            name='ntw',
            marker=dict(symbol='circle-dot',
                                        size=15,
                                        color='#ff0000',
                                        line=dict(color='rgb(50,50,50)', width=0.5)
                                        ),
            text=labels2,
            hoverinfo='text'
            )

    line1=Scatter(x=Xe1,
            y=Ye1,
            mode='lines',
            line= dict(color='rgb(252, 3, 3)', width=1),
            hoverinfo='none'
            )

    line2=Scatter(x=Xe2,
            y=Ye2,
            mode='lines',
            line= dict(color='rgb(210,210,210)', width=1),
            hoverinfo='none'
            )

    # Hide axis information
    axis=dict(showline=False, 
            zeroline=False,
            showgrid=False,
            showticklabels=False,
            title='' 
            )

    width=800
    height=800
    layout_plotly=Layout(title= "Scambio messaggi inter-alleanze",  
        font= dict(size=12),
        showlegend=False,
        autosize=False,
        width=width,
        height=height,
        xaxis=layout.XAxis(axis),
        yaxis=layout.YAxis(axis),          
        margin=layout.Margin(
            l=40,
            r=40,
            b=85,
            t=100,
        ),
        hovermode='closest'        
        )

    data=[line1, line2, marker1, marker2]
    return Figure(data=data, layout=layout_plotly)

def plotly_figure3(subgraph_day):
    all_labels=list(subgraph_day.vs['label'])
    n_nodes=len(all_labels)
    layt=subgraph_day.layout_fruchterman_reingold() #kamada-kawai layout

    alliances = []
    for node in subgraph_day.vs:
        if node["ally_id"] not in alliances: alliances.append(node["ally_id"])

    nodes_data = []
    for alliance in alliances:
        xn = []; yn = []; labels =[]
        k = 0
        for node in subgraph_day.vs:
            if node["ally_id"] == alliance:
                xn.append(layt[k][0])
                yn.append(layt[k][1])
                labels.append(alliance)
            k += 1
        color = '#{:06x}'.format(random.randint(0, 256**3))
        nodes_data.append((xn,yn,color,labels))

    edge_list=[e.tuple for e in subgraph_day.es]

    # extraxt edges positions
    Xe1=[]; Ye1=[]
    for e in edge_list:
        Xe1+=[layt[e[0]][0],layt[e[1]][0], None]
        Ye1+=[layt[e[0]][1],layt[e[1]][1], None]

    line=Scatter(x=Xe1,
                y=Ye1,
                mode='lines',
                line= dict(color='rgb(252, 3, 3)', width=1),
                hoverinfo='none'
                )

    datas = []
    datas.append(line)
    for el in nodes_data:
        marker=Scatter(x=el[0],
                    y=el[1],
                    mode='markers',
                    name='ntw',
                    marker=dict(symbol='circle-dot',
                                                size=10,
                                                color=el[2],
                                                line=dict(color='rgb(50,50,50)', width=0.5)
                                                ),
                    text=el[3],
                    hoverinfo='text'
                    )
        datas.append(marker)

    # Hide axis information
    axis=dict(showline=False, 
            zeroline=False,
            showgrid=False,
            showticklabels=False,
            title='' 
            )

    width=800
    height=800
    layout_plotly=Layout(title= "attacchi inter-alleanza",  
        font= dict(size=12),
        showlegend=False,
        autosize=False,
        width=width,
        height=height,
        xaxis=layout.XAxis(axis),
        yaxis=layout.YAxis(axis),          
        margin=layout.Margin(
            l=40,
            r=40,
            b=85,
            t=100,
        ),
        hovermode='closest'        
        )
    fig=Figure(data=datas, layout=layout_plotly)
    return fig

def plotly_figure45(subgraph_day, ally, flag_internals, day):

    with open (f'pickles/differences{ally}_day{day}', 'rb') as fp: 
        exits = pickle.load(fp)
    if flag_internals: testo_annotation=f'Giocatori usciti dall\'alleanza: {exits}'
    else: testo_annotation = ""
    
    labels=list(subgraph_day.vs['label'])
    n_nodes=len(labels)
    edge_list=[e.tuple for e in subgraph_day.es]
    layt=subgraph_day.layout_fruchterman_reingold()
    labels1 = []
    labels2 = []
    Xn1 = []; Yn1 = []; Xn2 = []; Yn2 = []
    for k in range(0,n_nodes):
        node = subgraph_day.vs[k]
        if node['ally_id'] != None:
            Xn1.append(layt[k][0])
            Yn1.append(layt[k][1])
            labels1.append(labels[k])
        else:
            Xn2.append(layt[k][0])
            Yn2.append(layt[k][1])
            labels2.append(labels[k])

    Xe=[];Ye=[]
    for e in edge_list:
        Xe+=[layt[e[0]][0],layt[e[1]][0], None]
        Ye+=[layt[e[0]][1],layt[e[1]][1], None]

    #plot edges and nodes
    trace1=Scatter(x=Xe,
                y=Ye,
                mode='lines',
                line= dict(color='rgb(210,210,210)', width=1),
                hoverinfo='none'
                )

    trace2=Scatter(x=Xn1,
                y=Yn1,
                mode='markers',
                name='ntw',
                marker=dict(symbol='circle-dot',
                                            size=10,
                                            color='#ff0000',
                                            line=dict(color='rgb(50,50,50)', width=0.5)
                                            ),
                text=labels1,
                hoverinfo='text'
                )

    trace3=Scatter(x=Xn2,
                y=Yn2,
                mode='markers',
                name='ntw',
                marker=dict(symbol='circle-dot',
                                            size=5,
                                            color='#6959CD',
                                            line=dict(color='rgb(50,50,50)', width=0.5)
                                            ),
                text=labels2,
                hoverinfo='text'
                )
    # Hide axis information
    axis=dict(showline=False, 
            zeroline=False,
            showgrid=False,
            showticklabels=False,
            title='' 
            )

    width=800
    height=800
    layout_plotly=Layout(title= f"attacchi alleanza {ally}",  
        font= dict(size=12),
        showlegend=False,
        autosize=False,
        width=width,
        height=height,
        xaxis=layout.XAxis(axis),
        yaxis=layout.YAxis(axis),          
        margin=layout.Margin(
            l=40,
            r=40,
            b=85,
            t=100,
        ),
        hovermode='closest',
        annotations=[
            dict(
            showarrow=False, 
                text = testo_annotation,  
                xref='paper',     
                yref='paper',     
                x=0,  
                y=0,  
                xanchor='left',   
                yanchor='bottom',  
                font=dict(
                size=14 
                )     
                )
            ]
    )
    
    data=[trace1, trace2, trace3]
    fig=Figure(data=data, layout=layout_plotly)
    return fig
