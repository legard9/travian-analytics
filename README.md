- **Dataset** contains all the data given for the Uni project

- **Analysis.ipynb** is the source code for all the anlysis made with the dataset

- **dash_presentation.py** is the source code to start the interactive dash presentation

- **Dash_plotly.py** contains some of the plotly plots needed for the presentation

- **Presentazione.pdf** is the presentation i used for the exam in addition to the dash presentation

- **Relazione.pdf** is the full report of the analytics work

- **Presentazione.pdf** is the presentation i used for the exam in addition to the dash presentation

