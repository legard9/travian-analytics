import dis
import dash
import dash_core_components as dcc
import dash_table
import dash_html_components as html
from dash.dependencies import Input, Output
import pickle
import plotly.graph_objs as go
import chart_studio
from plotly.graph_objs import *
import chart_studio.plotly as py
from torch import layout
chart_studio.tools.set_credentials_file(username='legard9', api_key='HlqO8DxyJd2tIevOfhf9')
import DashPlotly
import pandas as pd

with open ('pickles/ally_number_through_time', 'rb') as fp: ally_number_through_time = pickle.load(fp)
with open ('pickles/n_trades', 'rb') as fp: n_trades = pickle.load(fp)
with open ('pickles/n_attacks', 'rb') as fp: n_attacks = pickle.load(fp)
with open ('pickles/n_messages', 'rb') as fp: n_messages = pickle.load(fp)
with open ('pickles/n_nodes_trades', 'rb') as fp: n_nodes_trades = pickle.load(fp)
with open ('pickles/n_nodes_attacks', 'rb') as fp: n_nodes_attacks = pickle.load(fp)
with open ('pickles/n_nodes_messages', 'rb') as fp: n_nodes_messages = pickle.load(fp)
with open ('pickles/stable_alliances', 'rb') as fp: stable_alliances_list = pickle.load(fp)
with open ('pickles/trades_means_df', 'rb') as fp: trades_means_df = pickle.load(fp)
with open ('pickles/attacks_means_df', 'rb') as fp: attacks_means_df = pickle.load(fp)
with open ('pickles/assortativity_messages', 'rb') as fp: assortativity_messages = pickle.load(fp)
with open ('pickles/picked_alliances', 'rb') as fp: picked_alliances = pickle.load(fp)
df = pd.DataFrame()

# Using 'Address' as the column name
# and equating it to the list
df['giorno'] = range(1,31)
df['alleanze'] = ally_number_through_time
df['scambi commerciali'] = n_trades
df['attacchi'] = n_attacks
df['messaggi'] = n_messages

columns_names = attacks_means_df.columns
alliances = [37,102,72,151,8,146,50,116,23,185,27,22,122,58,15,198,136,80]

def distribution_barplots(day,ally):
    with open (f'pickles/attack_distribution_ally{ally}_day{day}', 'rb') as fp: 
        distribution = pickle.load(fp)
    figure={
                'data': [
                    {'x': distribution[0],
                        'y': distribution[1],
                        'type': 'bar',
                        'name': 'attacchi subiti'
                    },
                    {'x': distribution[2],
                        'y': distribution[3],
                        'type': 'bar',
                        'name': 'attacchi eseguiti'
                    },],
                'layout': go.Layout(
                        xaxis={
                            'title': 'numero di attacchi',
                            'type': 'log',
                        },
                        yaxis={
                            'title': 'frequenza',
                            "type": "log"
                        },
                        hovermode='closest'
                    )
                }
    return figure

def extract_stable_alliances(value):
    stable_alliances = []
    for el in stable_alliances_list:
        if(el[-1]>value):
            tmp = {
                'x': list(range(1,30)),
                'y': el[:30],
                'type': 'line',
                'name': el[-2]
            }
            stable_alliances.append(tmp)
    return stable_alliances

external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']

app = dash.Dash(__name__, external_stylesheets=external_stylesheets)

app.layout = html.Div([
            html.H3('Statistiche Generali Dataset', style={'width': '100%','textAlign': 'center'}),
            dcc.Graph(
                    id='graph-1-tabs',
                    figure={
                        'data': [
                            {'x': list(range(1,31)),
                                'y': ally_number_through_time,
                                'type': 'line',
                                'name': 'alleanze'
                            }, 
                            {'x': list(range(1,31)),
                                'y': n_trades,
                                'name': 'scambi commerciali',
                                'type': 'line'
                            }, 
                            {'x': list(range(1,31)),
                                'y': n_messages,
                                'name': 'messaggi',
                                'type': 'line'
                            }, 
                            {'x': list(range(1,31)),
                                'y': n_attacks,
                                'name': 'attacchi',
                                'type': 'line'
                            },
                            {'x': list(range(1,31)),
                                'y': n_nodes_trades,
                                'name': 'utenti con scambi commerciali',
                                'type': 'line'
                            }, 
                            {'x': list(range(1,31)),
                                'y': n_nodes_messages,
                                'name': 'utenti con messaggi',
                                'type': 'line'
                            }, 
                            {'x': list(range(1,31)),
                                'y': n_nodes_attacks,
                                'name': 'utenti in attacchi',
                                'type': 'line'
                            }],
                        'layout': go.Layout(
                                xaxis={
                                    'title': 'giorno del mese',
                                },
                                yaxis={
                                    'title': 'records',
                                },
                                hovermode='closest'
                            )
                        }
            , style={'width': '100%'}),
            
            html.H3('Alleanze Stabili', style={'width': '100%','textAlign': 'center', "margin-top": "200px"}),
            
            dcc.Graph(id='graph-2'),
            html.Div([
                dcc.Slider(
                    id='slider1',
                    min=0,
                    max=60,
                    value=2,
                    step=10,
                    marks={str(n): str(n) for n in range(0,61,5)})
            ], style={'width': '60%','textAlign': 'center', 'margin-left':'400px'}),
            

            html.H3(children='Grafo messaggi', style={'width': '100%','textAlign': 'center', "margin-top": "200px"}),
            html.Div([
                html.Div([
                    html.P(children='Giorno', style={'width': '100%','textAlign': 'center'})
                ]),
                html.Div([
                    dcc.Dropdown(
                        id='xaxis-column-graph1',
                        options=[{'label': i, 'value': i} for i in list(range(1,31))],
                        value='3'
                    ),
                ])
            ], style = {'margin-left':"870px", 'display': 'inline-block','width': '8%'}),

            html.Div([
                html.Div([
                    html.P(children='Alleanza', style={'width': '100%','textAlign': 'center'})
                ]),
                html.Div([
                    dcc.Dropdown(
                        id='yaxis-column-graph1',
                        options=[{'label': i, 'value': i} for i in alliances],
                        value='122'
                    )])
            ], style = {'display': 'inline-block','width': '8%'}),
            
            html.Div([
                dcc.RadioItems(
                    id='radio1',
                    options=[
                        {'label': 'Nascondi Capi', 'value': 0},
                        {'label': 'Mostra Capi', 'value': 1}
                        ], value=1
                )
            ], style={'margin-left': '1000px'}),

            dcc.Graph(id='graph1', style={"margin-left": "600px","margin-right": "600px",}),

            html.H3(children='Messaggi inter alleanze', style={'width': '100%','textAlign': 'center', "margin-top": "200px"}),
            
            html.Div([
                html.Div([
                    html.P(children='Giorno', style={'width': '100%','textAlign': 'center'})
                ]),
                html.Div([
                    dcc.Dropdown(
                    id='xaxis-column-graph2',
                    options=[{'label': i, 'value': i} for i in list(range(1,31))],
                    value='3')
                ])
            ], style = {'margin-left':"950px", 'display': 'inline-block','width': '8%'}),
            

            html.Div([
                dcc.RadioItems(
                id='radio2',
                options=[
                {'label': 'Nascondi Capi', 'value': 0},
                {'label': 'Mostra Capi', 'value': 1}
                ], value=1
            )], style={'width': '20%', 'margin-left':'950px'}),

            dcc.Graph(id='graph2', style={"margin-left": "600px","margin-right": "600px",}),

            html.H3(children='Attacchi inter alleanze', style={'width': '100%','textAlign': 'center', "margin-top": "200px"}),
            
            html.Div([
                html.Div([
                    html.P(children='Giorno', style={'width': '100%','textAlign': 'center'})
                ]),
                html.Div([
                    dcc.Dropdown(
                    id='xaxis-column-graph3',
                    options=[{'label': i, 'value': i} for i in list(range(1,31))],
                    value='3')
                ])
            ], style = {'margin-left':"950px", 'display': 'inline-block','width': '8%'}),
            
            dcc.Graph(id='graph3', style={"margin-left": "600px","margin-right": "600px",}),

            html.H3(children='Statistiche Attacchi Alleanze', style={'width': '100%','textAlign': 'center', "margin-top": "200px"}),
            html.Div([
                dcc.Dropdown(
                    id='xaxis-column',
                    options=[{'label': i, 'value': i} for i in columns_names],
                    value='attacchi_subiti'
                )], style={'width': '20%', 'display': 'inline-block', 'margin-left':'650px'}),

            html.Div([
                dcc.Dropdown(
                    id='yaxis-column',
                    options=[{'label': i, 'value': i} for i in columns_names],
                    value='attacchi_eseguiti'
                )], style={'width': '20%', 'display': 'inline-block'}),
                

            dcc.Graph(id='scatter1'),

            html.H3(children='Analisi attacchi alleanze notevoli', style={'width': '100%','textAlign': 'center', "margin-top": "200px"}),
            
            html.Div([
                html.Div([
                    html.P(children='Giorno', style={'width': '100%','textAlign': 'center'})
                ]),
                html.Div([
                    dcc.Dropdown(
                    id='xaxis-day-g45',
                    options=[{'label': i, 'value': i} for i in list(range(1,31))],
                    value='3'
                    )
                ])
            ], style = {'margin-left':"800px", 'display': 'inline-block','width': '8%'}),

            html.Div([
                html.Div([
                    html.P(children='Alleanza1', style={'width': '100%','textAlign': 'center'})
                ]),
                html.Div([
                    dcc.Dropdown(
                    id='xaxis-ally1-g45',
                    options=[{'label': i, 'value': i} for i in picked_alliances],
                    value='72')
                ])
            ], style = {'display': 'inline-block','width': '8%'}),

            html.Div([
                html.Div([
                    html.P(children='Alleanza2', style={'width': '100%','textAlign': 'center'})
                ]),
                html.Div([
                    dcc.Dropdown(
                    id='xaxis-ally2-g45',
                    options=[{'label': i, 'value': i} for i in picked_alliances],
                    value='23')
                ])
            ], style = {'display': 'inline-block','width': '8%'}),

            html.Div([
                dcc.RadioItems(
                id='radio45',
                options=[
                {'label': 'Mostra attacchi esterni', 'value': 0},
                {'label': 'Nascondi attacchi esterni', 'value': 1}
                ], value=0
            )], style={'margin-left':"950px", 'width': '20%'}),

            html.Div([
                dcc.Graph(id='graph4')
            ], style={'width': '50%', 'display': 'inline-block'}),

            html.Div([
                dcc.Graph(id='graph5')
            ], style={'width': '50%', 'display': 'inline-block'}),
            
            html.Div([
                dcc.Graph(id='barplot1')
            ], style={'width': '50%', 'display': 'inline-block'}),

            html.Div([
                dcc.Graph(id='barplot2')
            ], style={'width': '50%', 'display': 'inline-block'}),

],style={'align-items':'center'})


@app.callback(Output('graph1', 'figure'),
                [Input('xaxis-column-graph1', 'value'),
                Input('yaxis-column-graph1', 'value'),
                Input('radio1', 'value')])
def update_plotly(day, ally_id, show_boss):
    with open (f'pickles/messages_intra_ally{ally_id}_day{day}', 'rb') as fp: 
        graph = pickle.load(fp)
    fig = DashPlotly.plotly_figure(graph, show_boss)
    return fig

@app.callback(Output('graph2', 'figure'),
                [Input('xaxis-column-graph2', 'value'),
                Input('radio2', 'value')])
def update_plotly(day, show_boss):
    with open (f'pickles/messages_inter_ally_day{day}', 'rb') as fp: 
        graph = pickle.load(fp)
    fig = DashPlotly.plotly_figure2(graph, show_boss)
    return fig

@app.callback(Output('graph3', 'figure'),
                [Input('xaxis-column-graph3', 'value')])
def update_plotly(day):
    with open (f'pickles/attacks_inter_ally_day{day}', 'rb') as fp: 
        graph = pickle.load(fp)
    fig = DashPlotly.plotly_figure3(graph)
    return fig

@app.callback(Output('graph4', 'figure'),
                [Input('xaxis-day-g45', 'value'),
                Input('xaxis-ally1-g45', 'value'),
                Input('radio45', 'value')])
def update_plotly(day,ally1,flag_internals):
    if flag_internals:
        with open (f'pickles/attacks_internal_ally{ally1}_day{day}', 'rb') as fp: 
            graph = pickle.load(fp)
    else:
        with open (f'pickles/attacks_ally{ally1}_day{day}', 'rb') as fp: 
            graph = pickle.load(fp)
    fig = DashPlotly.plotly_figure45(graph,ally1,flag_internals, day)
    return fig

@app.callback(Output('graph5', 'figure'),
                [Input('xaxis-day-g45', 'value'),
                Input('xaxis-ally2-g45', 'value'),
                Input('radio45', 'value')])
def update_plotly(day,ally2,flag_internals):
    if flag_internals:
        with open (f'pickles/attacks_internal_ally{ally2}_day{day}', 'rb') as fp: 
            graph = pickle.load(fp)
    else:
        with open (f'pickles/attacks_ally{ally2}_day{day}', 'rb') as fp: 
            graph = pickle.load(fp)
    fig = DashPlotly.plotly_figure45(graph,ally2, flag_internals, day)
    return fig

@app.callback(Output('barplot1', 'figure'),
                [Input('xaxis-day-g45', 'value'),
                Input('xaxis-ally1-g45', 'value')])
def update_plotly(day,ally):
    return distribution_barplots(day,ally)
    
@app.callback(Output('barplot2', 'figure'),
                [Input('xaxis-day-g45', 'value'),
                Input('xaxis-ally2-g45', 'value')])
def update_plotly(day,ally):
    return distribution_barplots(day,ally)

@app.callback(Output('graph-2', 'figure'),
              [Input('slider1', 'value')])
def update_graph2(slider_value):
    return {
        'data': extract_stable_alliances(slider_value),
        'layout': go.Layout(
                                xaxis={
                                    'title': 'giorno del mese',
                                },
                                yaxis={
                                    'title': 'membri',
                                },
                                hovermode='closest'
                            )
    }

@app.callback(
        Output('scatter1', 'figure'),
        [Input('xaxis-column', 'value'),
         Input('yaxis-column', 'value')])

def update_graph(xaxis_column_name, yaxis_column_name):

    dff = attacks_means_df

    return {
        'data': [go.Scatter(
                x=dff[xaxis_column_name],
                y=dff[yaxis_column_name],
                text=dff['clan_id'],
                mode='markers',
                marker={
                    'size': 15,
                    'opacity': 0.5,
                    'line': {'width': 0.5, 'color': 'white'}
                }
        )],
        'layout': go.Layout(
                xaxis={
                    'title': xaxis_column_name,
                    'type': 'linear'
                },
                yaxis={
                    'title': yaxis_column_name,
                    'type': 'linear'
                },
                margin={'l': 40, 'b': 40, 't': 10, 'r': 0},
                hovermode='closest'
        )
    }

if __name__ == '__main__':
    app.run_server(debug=True)



